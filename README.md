# Wings XI Web
Wings XI Web Front-End Interface

Wings XIWeb is a front-end interface for the Wings FFXI server. When installed, it can be used to offer a web portal for
use with your FFXI private server.

# Features
* See a list of who is online currently
* See what's for sale in the Auction House (limited AH to single item listing instead of every single item)
* See your character's and inventory per character
* View everyones listed bazaar items
* Dark Theme & Light Theme 
* Mobile Accessibility

# Planned Features
* GM Ticket page
* Voting Pages
* Maps with character positions
* Manage server settings within Admin Panel
* Multi-Account support via Email linking

This project is still being developed, and any changes will be added to this file.

# TODO
* Finish server accounts page
* Create Admin Panel page

Credit to Whasf for the base website 
