$(document).ready(function() {

$.fn.dataTable.ext.errMode = 'none';
	//When the DOM is ready, initialize the dataTable to show who is online
    var bazaarTable = $('#bazaarList').DataTable({
    	processing: true,
        serverSide: true,
        searching: false,
        ajax: "services/getBazaarList.php",
        columns: [
			{orderable: false, width: "10%"},
			null,
			null,
			{width: "10%"},
			null,
			{orderable: false},
			{orderable: false}
		],
		//order: [[ 1, "asc" ]],
		processing: false
    });

    $.get("services/getBazaarCount.php", function(data){
        $("#bazaarH2").text("There are " + parseFloat($.trim(data)).toLocaleString('en') + " items in the Bazaar List.");
    });

    //Every 10 seconds, refresh the table
    setInterval( function () {
    	$.get("services/getBazaarCount.php", function(data){
        	$("#bazaarH2").text("There are " + parseFloat($.trim(data)).toLocaleString('en') + " items in the Bazaar List.");
    	});
    	bazaarTable.ajax.reload(null, false);
	}, 10000 );


} );