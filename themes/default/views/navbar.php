<?php

	$output .= '
		<body>
				
				<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
					<div class="container-fluid">
			      	<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#mainNavbarContent" aria-controls="mainNavbarContent" aria-expanded="false" aria-label="Toggle navigation">
      					<span class="navbar-toggler-icon"></span>
    				</button>

			      
			      	<div class="collapse navbar-collapse" id="mainNavbarContent">
			        	<ul class="navbar-nav me-auto">
			        		<li class="nav-item">
			        			<a id="navHomebtn" class="nav-link" href="index.php">Home<span class="sr-only">(current)</span></a>
			        		</li>
			        		<li class="nav-item">
			        			<a id="navRulesbtn" class="nav-link" href="rules.php">Rules<span class="sr-only">(current)</span></a>
			        		</li>
			        		<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" id="navbarDownload" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Downloads</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="http://www.playonline.com/ff11us/download/media/install_win.html">Final Fantasy XI Download</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="https://ashitaxi.com/">Ashita Download</a>
								<a class="dropdown-item" href="https://www.windower.net/">Windower Download</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="downloads/FFXI-UpdatePatch.zip">FFXI Update Patch</a>
								<a class="dropdown-item" href="downloads/ffxi_d3d8to9_proxy_v1.1.0.0.7z">FFXI Direct3D 8 to 9 Proxy</a>
								<a class="dropdown-item" href="downloads/wingsloader.zip">FFXI Wings Loader</a>
								</div>
							</li>
			          		<li class="nav-item">
			            		<a class="nav-link" href="roster.php">Roster<span class="sr-only"></span></a>
			          		</li>
			          		<li class="nav-item">
			            		<a class="nav-link" href="auctionHouse.php">Auction House<span class="sr-only"></span></a>
			          		</li>
			          		<li class="nav-item">
			            		'.(!empty($_SESSION['logged']) ? '<a class="nav-link" href="bazaar.php">Bazaar<span class="sr-only"></span></a>' : '' ).'
			          		</li>
			          		<li class="nav-item">
			            		'.(!empty($_SESSION['logged']) && ($_SESSION['auth']['username'] == $admin) ? '<a class="nav-link" href="#">Admin Panel<span class="sr-only"></span></a>' : '' ).'
			          		</li>
			          	<!--<form class="form-inline my-2 my-lg-0">
				          <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
				          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
				        </form>-->
			        	</ul>
			        <ul class="navbar-nav ms-auto">
			        ';

	if(!empty($_SESSION['logged']) && $_SESSION['logged'] == TRUE){
		$output .= '
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbarUsername" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i>  '.$_SESSION['auth']['username'].'</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
				<a class="dropdown-item" href="myAccount.php">My Account</a>
				<a class="dropdown-item" href="serverregister.php">Register for Server</a>
				<a class="dropdown-item" href="myCharacters.php">My Characters</a>
				</div>
			</li>
		';
		$output .= '
				        <a class="nav-link text-danger d-flex" href="logout.php">Logout<span class="sr-only">(current)</span></a>
				        </ul>
				      </div>
				    </div>
				</nav>
		';
	} else {

		$output .= '
				        <a class="nav-link d-flex" href="login.php">Login<span class="sr-only">(current)</span></a>
				        '.($allowAccountRegistration ? '<a id="navRegisterLink" class="nav-link d-flex" href="register.php"><i>Register</i></a>' : '' ).'
				        </ul>
				      </div>
				    </div>
				</nav>
		';

	}

	

?>
