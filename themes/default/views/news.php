<?php

	$output .= '
		<div id="bannerBackground" class="jumbotron jumbotron-fluid">
	        <div class="container">
	          <h1 class="display-3">'.$frontpage_title.'</h1>
	          <p>'.$frontpage_message.'</p>
	        </div>
	      </div>
	';

	$output .= '
		<div id="newsContainer" class="container">
			<div class="page-header">
  				<h1>'.$newsTitle.'</h1>
  				'.$newsDetails.'
			</div>
		</div>
	';
?>