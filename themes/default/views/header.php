<?php

	$output = '
		<!DOCTYPE html>
		<html>
			<head>
				<!-- Required meta tags -->
			    <meta charset="utf-8">
			    <meta name="viewport" content="width=device-width, initial-scale=1.0">
				<title>'.$site_name.'</title>

				<script type="text/javascript" src="themes/default/js/all.min.js"></script>
				<script src="themes/default/js/jquery-3.6.0.min.js"></script>
				<script src="themes/default/js/popper.min.js"></script>
				<script src="themes/default/js/bootstrap.bundle.min.js"></script>
				<script type="text/javascript" src="themes/default/js/jquery.dataTables.min.js"></script>
				<script type="text/javascript" src="themes/default/js/tooltipster.bundle.min.js"></script>
				
				<link rel="stylesheet" type="text/css" href="themes/default/css/styles.css"/>
				<link rel="stylesheet" type="text/css" href="themes/default/css/tooltipster.bundle.min.css"/>
				<link rel="stylesheet" href="themes/default/css/bootstrap.min.css">
				<link rel="stylesheet" type="text/css" href="themes/default/css/jquery.dataTables.min.css"/>
				<link rel="apple-touch-icon" sizes="180x180" href="themes/default/images/apple-touch-icon.png">
				<link rel="icon" type="image/png" sizes="32x32" href="themes/default/images/favicon-32x32.png">
				<link rel="icon" type="image/png" sizes="16x16" href="themes/default/images/favicon-16x16.png">
				<link rel="manifest" href="/site.webmanifest">
								
				<script src=\'https://www.google.com/recaptcha/api.js\'></script>

			</head>
	';

?>