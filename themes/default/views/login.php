<?php

	$output .= '

		<div id="bannerBackground" class="jumbotron jumbotron-fluid">
	        <div class="container">
	          <h1 class="display-3">'.$frontpage_title.'</h1>
	          <p>'.$frontpage_message.'</p>
	        </div>
	     </div>
	 ';

	//If there were errors, let's show them.
	if(!empty($_SESSION['errors']['login'])){
		
		foreach ($_SESSION['errors']['login'] as $error) {
     		$output .= '
				<div id="alertContainer" class="container alert alert-danger">
					'.$error.'
				</div>
			';
    	}

	}

	$output .= '

		<div id="loginContainer" class="container">
			<div id="loginCard" class="card bg-light mb-3">
				<div class="card-header">Login</div>
				<div class="card-body">
					<form method="post" action="./login.php">
						<input type="hidden" name="login" value="1" />
						<div class="form-group">
							<label for="">Username</label>
							<input type="text" class="form-control" name="username" placeholder="">
						</div>
						<div id="buttonSpacing" class="form-group">
							<label for="">Password</label>
							<input type="password" class="form-control" name="password" placeholder="">
						</div>
						<button type="submit" class="btn btn-primary">Login</button>
						'.($allowAccountRegistration ? '<a id="loginRegisterBtnspace" class="btn btn-success" href="register.php" role="button">Register</a>' : '' ).'
					</form>
				</div>
			</div>
		</div>
	';

?>