<?php

	//If there were errors, let's show them.
	if(!empty($_SESSION['errors']['login'])){
		
		foreach ($_SESSION['errors']['login'] as $error) {
     		$output .= '
				<div id="alertContainer" class="container alert alert-danger">
					'.$error.'
				</div>
			';
    	}

	}

	$output .= '

		<div id="bannerBackground" class="jumbotron jumbotron-fluid">
        <div class="container">
          <h1 class="display-3">'.$frontpage_title.'</h1>
          <p>'.$frontpage_message.'</p>
        </div>
      </div>

<div id="bazaarContainer" class="container">
			<h2 id="bazaarH2">There are 0 items in the Bazaar List.</h2>
			<p></p>
			<table id="bazaarList" class="display" cellspacing="0" width="100%">
				<thead>
					<th>Icon</th>
					<th>Item Name</th>
					<th>Character</th>
					<th>Location</th>
					<th>Price</th>
					<th></th>
					<th></th>
				</thead>
				<tfoot>
					<th>Icon</th>
					<th>Item Name</th>
					<th>Character</th>
					<th>Location</th>
					<th>Price</th>
					<th></th>
					<th></th>
				</tfoot>
			</table>
		</div>
		<p style="padding:50px;">
		<script type="text/javascript" src="themes/default/js/bazaar.js"></script>
	';

?>