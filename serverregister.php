<?php

	//Check if the config.php file has already been created.
	//If so, include it, otherwise display an error message
	if (file_exists('config.php')) {
		include_once('config.php');
	} else {
		$_SESSION = [];
		$_SESSION['errors']['general'][] = $lang['error']['config'];
	}

	//Get the reCAPTCHA PHP Library in case we need it
	//Source https://github.com/google/recaptcha
	require('includes/Recaptcha/src/autoload.php');

	//If the system is installed, proceed
	//'INSTALLED' is defined in the config.php file
	//If the system is not installed, navigate to the install page
	if (defined('INSTALLED')) {

		//includes.php
		include_once('./lang/'.$language.'.inc.php');
		include_once('includes/includes.php');
  		include_once('includes/functions.php');

  		unset($_SESSION['messages']);
		unset($_SESSION['errors']);

		$_SESSION['messages']['registration'] = '';
		$_SESSION['errors']['registration'] = '';

		// This makes sure the a user is logged in or it boots them back to the Main page
		//if (empty($_SESSION['logged'] == true)){
		//	header("Location: ../index.php");
		//}

		//Check to see if registration is allowed.  If not, continue no further
		if ($allowAccountRegistration == FALSE){
			$_SESSION = [];
			$_SESSION['errors']['registration'][] = $lang['error']['registration']['not_allowed'];

		}

		//If this is a post back from the register form, let's see if we can process it or if we need to return some errors
  		if (!empty($_POST['register']) && $allowAccountRegistration) {

  			//Grab the post parameters
  			$username2 = $_POST['username2'];
  			$password3 = $_POST['password3'];
  			$password4 = $_POST['password4'];
  			$email2 = $_POST['email2'];

  			//Check reCAPTCHA to make sure it's not a bot or something
  			if($useRecaptcha){
  				$gRecaptchaResponse = $_POST['g-recaptcha-response'];
	  			$remoteIp = getRealIPAddr();
	  			$recaptcha = new \ReCaptcha\ReCaptcha($recaptchaSecretKey);
				$resp = $recaptcha->verify($gRecaptchaResponse, $remoteIp);
  			}

			if ($useRecaptcha == FALSE || $resp->isSuccess()) {
			    // verified!
			    // if Domain Name Validation turned off don't forget to check hostname field
			    // if($resp->getHostName() === $_SERVER['SERVER_NAME']) {  }

				//Do some error checking
	  			if(empty($username2) || empty($password3) || empty($password4)){

	  				if(empty($username2)){
	  					$_SESSION = [];
	  					$_SESSION['errors']['registration'][] = $lang['error']['registration']['user_empty2'];
	  				}

	  				if(empty($password3)){
	  					$_SESSION = [];
	  					$_SESSION['errors']['registration'][] = $lang['error']['registration']['password_empty2'];
	  				}

	  				if(empty($password4)){
	  					$_SESSION = [];
	  					$_SESSION['errors']['registration'][] = $lang['error']['registration']['password_empty2'];
	  				}

	  			} else {

	  				//If the user name exists, tell the user and don't register
		  			if(getServerAccountID($username2) > 0){
		  				$_SESSION = [];
		  				$_SESSION['errors']['registration'][] = $lang['error']['registration']['user_exists2'];
		  			} else 	if($password3 != $password4){
		  				//Check if the passwords match
		  				$_SESSION = [];
		  				$_SESSION['errors']['registration'][] = $lang['error']['registration']['password_match2'];	
		  			} else {

		  				if(createServerAccount($username2, $password3, $email2)) {
		  					$_SESSION = [];
		  					$_SESSION['messages']['registration'][] = $lang['messages']['registration']['account_created2'];
		  					$username2 = '';
		  					$email2 = '';
		  				} else {
		  					$_SESSION = [];
		  					$_SESSION['errors']['registration'][] = $lang['error']['registration']['general_error'];
		  				}

		  			}

	  			}

			} else {
			    $errors = $resp->getErrorCodes();
			    $_SESSION = [];
			    $_SESSION['errors']['registration'][] = $lang['error']['registration']['reCAPTCHA'];			    
			}

  		} else {

  			$username2 = '';
  			$email2 = '';

  		}

	} else {

		// If the config file exists, but the system is not installed, throw an error and redirect to the install directory
		$_SESSION = [];
  		$_SESSION['errors']['install'][] = $lang['error']['install']['not_installed'];
  		header("Location: install/index.php");

	}

	//These php files generate the html content to display
	include_once('themes/'.$theme.'/views/header.php');
	include_once('themes/'.$theme.'/views/navbar.php');
	include_once('themes/'.$theme.'/views/serverregister.php');
	include_once('themes/'.$theme.'/views/footer.php');
	echo $output;

?>